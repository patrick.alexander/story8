from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
import requests
import json



def home(request):
    return render(request, 'main/home.html')

def fungsi_suatu_url(request):
    response={}
    return render(request,'main/home.html', response)

def fungsi_data(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    kembalian = requests.get(url)
    data = json.loads(kembalian.content)
    #data = {'coba': 'ini isinya', 'sesuatu': 'ini sesuatu'}
    return JsonResponse(data, safe=False)

